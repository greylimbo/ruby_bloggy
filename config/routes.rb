Rails.application.routes.draw do

  get 'welcome/index'

  resources :stories do
    resources :comments
  end

  resources :comments do
    resources :comments
  end

  root 'welcome#index'

end
